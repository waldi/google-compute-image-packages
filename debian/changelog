google-compute-image-packages (20181206-4) UNRELEASED; urgency=medium

  * Only write daemon output to console.
  * Remove dependency clutter from systemd services.

 -- Bastian Blank <waldi@debian.org>  Wed, 30 Jan 2019 17:08:27 +0100

google-compute-image-packages (20181206-3) unstable; urgency=medium

  [ Bastian Blank ]
  * Set maintainer to Debian Cloud Team.

  [ Lucas Kanashiro ]
  * Add patch disabling boto by default

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Tue, 22 Jan 2019 15:52:09 -0200

google-compute-image-packages (20181206-2) unstable; urgency=medium

  * Add a patch fixing a python 3 issue when it tries to write to file

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Thu, 03 Jan 2019 11:14:31 -0200

google-compute-image-packages (20181206-1) unstable; urgency=medium

  * New upstream version 20181206
  * Remove patches already applied by upstream
  * Declare compliance with Debian Policy 4.3.0
  * Remove unused entries in debian/copyright

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Wed, 26 Dec 2018 12:07:01 -0200

google-compute-image-packages (20180905-2) unstable; urgency=medium

  * Add patch enabling jounald to forward logs to console
  * Install journald conf instead of rsyslog
  * Remove runtime dependency on rsyslog
  * Do not install apt.conf files
  * Do not install modprobe config file
  * Remove irqbalance conflict for now
  * Add patch removing WantedBy=sshd from instance-setup init script

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Thu, 20 Dec 2018 10:04:21 -0200

google-compute-image-packages (20180905-1) unstable; urgency=medium

  * Initial release (Closes: #908077)

 -- Lucas Kanashiro <lucas.kanashiro@collabora.com>  Mon, 15 Oct 2018 16:18:23 -0300
